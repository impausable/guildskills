-----------------------------------------------------------------------------------------------
-- Client Lua Script for GuildSkills
-- Copyright (c) NCsoft. All rights reserved
-----------------------------------------------------------------------------------------------
 
require "Window"
require "GameLib"
require "Apollo"
require "Spell"
require "Unit"
 
-----------------------------------------------------------------------------------------------
-- GuildSkills Module Definition
-----------------------------------------------------------------------------------------------
local GuildSkills = {} 
 
-----------------------------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------------------------
-- e.g. local kiExampleVariableMax = 999
 
-----------------------------------------------------------------------------------------------
-- Initialization
-----------------------------------------------------------------------------------------------
function GuildSkills:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self 

    -- initialize variables here
	self.tSavedData = {}
	self.tSchem = {}
	self.tSkillz = {}
	self.strPlayer = GameLib.GetPlayerUnit():GetName()
	
    return o
end

function GuildSkills:Init()
	local bHasConfigureFunction = false
	local strConfigureButtonText = ""
	local tDependencies = {
		-- "UnitOrPackageName",
	}
    Apollo.RegisterAddon(self, bHasConfigureFunction, strConfigureButtonText, tDependencies)
end
 

-----------------------------------------------------------------------------------------------
-- GuildSkills OnLoad
-----------------------------------------------------------------------------------------------
function GuildSkills:OnLoad()
    -- load our form file
	self.xmlDoc = XmlDoc.CreateFromFile("GuildSkills.xml")
	self.xmlDoc:RegisterCallback("OnDocLoaded", self)
end

-----------------------------------------------------------------------------------------------
-- GuildSkills OnDocLoaded
-----------------------------------------------------------------------------------------------
function GuildSkills:OnDocLoaded()

	if self.xmlDoc ~= nil and self.xmlDoc:IsLoaded() then
	    self.wndMain = Apollo.LoadForm(self.xmlDoc, "GuildSkillsForm", nil, self)
		if self.wndMain == nil then
			Apollo.AddAddonErrorText(self, "Could not load the main window for some reason.")
			return
		end
		
	    self.wndMain:Show(false, true)

		-- if the xmlDoc is no longer needed, you should set it to nil
		-- self.xmlDoc = nil
		
		-- Register handlers for events, slash commands and timer, etc.
		-- e.g. Apollo.RegisterEventHandler("KeyDown", "OnKeyDown", self)
		Apollo.RegisterSlashCommand("guildskills", "OnGuildSkillsOn", self)


		-- Do additional Addon initialization here
	end
end

-----------------------------------------------------------------------------------------------
-- GuildSkills Functions
-----------------------------------------------------------------------------------------------
-- Define general functions here

function GuildSkills:AddTrade(tSkill)
	self.tSkillz[tSkill.strName] = {}
	self.tSkillz[tSkill.strName]["active"] = tSkill.bIsActive
	self.tSkillz[tSkill.strName]["harvest"] = tSkill.bIsHarvesting
	self.tSkillz[tSkill.strName]["level"] = tSkill.eTier
	self.tSkillz[tSkill.strName]["name"] = tSkill.strName
	self.tSkillz[tSkill.strName]["xp"] = tSkill.nXp
	self.tSkillz[tSkill.strName]["nlvl"] = tSkill.nXpForNextTier

	self.tSavedData[self.strPlayer][tSkill.strName] = {}
	self.tSavedData[self.strPlayer][tSkill.strName]["active"] = tSkill.bIsActive
	self.tSavedData[self.strPlayer][tSkill.strName]["level"] = tSkill.eTier
	self.tSavedData[self.strPlayer][tSkill.strName]["xp"] = tSkill.nXp
	self.tSavedData[self.strPlayer][tSkill.strName]["nlvl"] = tSkill.nXpForNextTier
end

function GuildSkills:AddSchematic(tSkill, tSch)
	self.tSavedData[self.strPlayer][tSkill.strName][tSch.nSchematicId] = {}
	self.tSavedData[self.strPlayer][tSkill.strName][tSch.nSchematicId]["name"] = tSch.strName
	self.tSavedData[self.strPlayer][tSkill.strName][tSch.nSchematicId]["level"] = tSch.eTier
end

function GuildSkills:AddHarvest(tSkill)
	self.tSavedData[self.strPlayer][tSkill.strName]["active"] = tSkill.bIsActive
	self.tSavedData[self.strPlayer][tSkill.strName]["level"] = tSkill.eTier
	self.tSavedData[self.strPlayer][tSkill.strName]["xp"] = tSkill.nXp
	self.tSavedData[self.strPlayer][tSkill.strName]["nlvl"] = tSkill.nXpForNextTier

end

-- on SlashCommand "/guildskills"
function GuildSkills:OnGuildSkillsOn()
	Print(self.strPlayer)
	self.tSavedData[self.strPlayer] = {}
	Print(tostring(self.tSavedData[self.strPlayer]))

    for idx, tCurrTradeskill in ipairs(CraftingLib.GetKnownTradeskills()) do
		if idx ~= CraftingLib.CodeEnumTradeskill.Runecrafting and not CraftingLib.GetTradeskillInfo(tCurrTradeskill.eId).bIsHobby and not CraftingLib.GetTradeskillInfo(tCurrTradeskill.eId).bIsAutoLearn then
			local tCurrTradeskillInfo = CraftingLib.GetTradeskillInfo(tCurrTradeskill.eId)
			GuildSkills:AddTrade(tCurrTradeskillInfo)
			if tCurrTradeskillInfo.bIsHarvesting then
					GuildSkills:AddHarvest(tCurrTradeskillInfo)
					Print("HARVEST MOON")
			else
				for idx2, tSchematic in pairs(CraftingLib.GetSchematicList(tCurrTradeskill.eId, nil, nil, true)) do
					if tCurrTradeskillInfo.eTier >= tSchematic.eTier then
						if tSchematic.bIsKnown then
							GuildSkills:AddSchematic(tCurrTradeskillInfo,tSchematic)
						end
					end
				end
			end
		end
	end
	local s = ""
	for k,v in pairs(self.tSkillz) do
		Print(k)
		tUgh = self.tSavedData[self.strPlayer]
		for kk,vv in pairs(tUgh) do
			Print(kk .. " : " .. tostring(vv))
		end
		 
		for kk,vv in pairs(v) do
			Print(kk .. " : " .. tostring(vv))
		end
	end
	for k,v in pairs(self.tSavedData[self.strPlayer]) do
		Print(k["name"])
		tWhew = self.tSkillz[k]
		s = s .. k
		if tWhew["harvest"] then
			s = s .. " Level 2 " .. tWhew["xp"] .. "/" .. tWhew["nlvl"] .. "\n==============\n"
		else
			s = s .. "\n==============\n" 
			for kk,vv in pairs(v) do
				if kk ~= "active" and kk ~= "level" and kk ~= "xp" and kk ~= "nlvl" then
					s = s .. vv["name"] .. " (level " .. vv["level"] .. ")\n"
				end
			end
		end
	end
	self.wndMain:FindChild("TradeMatic"):SetText(s)
	self.wndMain:Invoke() -- show the window
	
end


-----------------------------------------------------------------------------------------------
-- GuildSkillsForm Functions
-----------------------------------------------------------------------------------------------
-- when the OK button is clicked
function GuildSkills:OnOK()
	-- load old file, update char info and save
	-- then trigger watcher
	self.wndMain:Close() -- hide the window
end

-- when the Cancel button is clicked
function GuildSkills:OnCancel()
	self.wndMain:Close() -- hide the window
end

function GuildSkills:OnRestore(eLevel, tData)
    if eLevel ~= GameLib.CodeEnumAddonSaveLevel.General then
        return nil
    end
--  self.tSavedData = tData
--	self:OnDelayedInitTimer()
end

function GuildSkills:OnSave(eLevel)
	if eLevel ~= GameLib.CodeEnumAddonSaveLevel.General then
        return nil
    end
		
    return self.tSavedData
end

-----------------------------------------------------------------------------------------------
-- GuildSkills Instance
-----------------------------------------------------------------------------------------------
local GuildSkillsInst = GuildSkills:new()
GuildSkillsInst:Init()
